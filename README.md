# TODO Application with Drag and Drop

![banner](https://todo-a.netlify.app/img/banner.png)

### Live demo
[Demo](https://todo-a.netlify.app)

### How it works
[YouTube](https://youtu.be/LzTRAC_YqCQ)

### Frontend
[Repository](https://gitlab.com/paprotsky/todo-app-frontend)
